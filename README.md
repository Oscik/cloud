### Initialize hosts:

```bash
ansible-playbook -i inventory/live.ini initialize_host.yml
```

### Deploy instance:

#### cookbook-django
```bash
ansible-playbook -i inventory/live.ini deploy_instance.yml --extra-vars="instance_id=oscik_cookbook-django_live"
```
#### cookbook-vue
```bash
ansible-playbook -i inventory/live.ini deploy_instance.yml --extra-vars="instance_id=oscik_cookbook-vue_live"
```
#### oscik
```bash
ansible-playbook -i inventory/live.ini deploy_instance.yml --extra-vars="instance_id=oscik_oscik_live"
```

### Generate and encrypt a random password:

```bash
pwgen -scn 40 1 | xargs echo -n | ansible-vault encrypt_string
```

### Decrypt password

Please remember to remove leading spaces from the vault string.
```bash
echo '$ANSIBLE_VAULT;1.1;AES256
01234567
98765432' | ansible-vault decrypt && echo
```

ansible-inventory -i inventory/hcloud.yml --list
ansible-playbook -i inventory/hcloud.yml initialize_host.yml --limit collabora01
